CREATE TABLE emp (
  emp_no NUMBER(4),
  e_name VARCHAR2(10),
  job VARCHAR2(9),
  mgr NUMBER(4),
  hire_date DATE,
  sal NUMBER(7, 2),
  comm NUMBER(7, 2),
  dept_no NUMBER(2)
);

CREATE TABLE dept (
  dept_no NUMBER(2),
  d_name VARCHAR2(14),
  loc VARCHAR2(14)
);

INSERT ALL
  INTO emp VALUES (7369, 'SMITH', 'CLERK', 7902, '17-DEC-1980', 800, NULL, 20)
  INTO emp VALUES (7499, 'ALLEN', 'SALESMAN', 7698, '20-FEB-1981', 1600, 300, 30)
  INTO emp VALUES (7521, 'WARD', 'SALESMAN', 7698, '22-FEB-1981', 1250, 500, 30)
  INTO emp VALUES (7566, 'JONES', 'MANAGER', 7839, '2-APR-1981', 2975, NULL, 20)
  INTO emp VALUES (7654, 'MARTIN', 'SALESMAN', 7698, '28-SEP-1981', 1250, 1400, 30)
  INTO emp VALUES (7698, 'BLAKE', 'MANAGER', 7839, '1-MAY-1981', 2850, NULL, 30)
  INTO emp VALUES (7782, 'CLARK', 'MANAGER', 7839, '9-JUN-1981', 2450, NULL, 10)
  INTO emp VALUES (7788, 'SCOTT', 'ANALYST', 7566, '09-DEC-1982', 3000, NULL, 20)
  INTO emp VALUES (7839, 'KING', 'PRESIDENT', NULL, '17-NOV-1981', 5000, NULL, 10)
  INTO emp VALUES (7844, 'TURNER', 'SALESMAN', 7698, '8-SEP-1981', 1500, 0, 30)
  INTO emp VALUES (7876, 'ADAMS', 'CLERK', 7788, '12-JAN-1983', 1100, NULL, 20)
  INTO emp VALUES (7900, 'JAMES', 'CLERK', 7698, '3-DEC-1981', 950, NULL, 30)
  INTO emp VALUES (7902, 'FORD', 'ANALYST', 7566, '3-DEC-1981', 3000, NULL, 20)
  INTO emp VALUES (7934, 'MILLER', 'CLERK', 7782, '23-JAN-1982', 1300, NULL, 10)
SELECT * FROM dual;

INSERT ALL
  INTO dept VALUES (10, 'ACCOUNTING', 'NEW YORK')
  INTO dept VALUES (20, 'RESEARCH', 'DALLAS')
  INTO dept VALUES (30, 'SALES', 'CHICAGO')
  INTO dept VALUES (40, 'OPERATIONS', 'BOSTON')
SELECT * FROM dual;

SELECT e_name
FROM emp
WHERE mgr = (
  SELECT emp_no
  FROM emp
  WHERE e_name = 'KING'
);

SELECT AVG(sal)
FROM emp
WHERE dept_no = (
  SELECT dept_no
  FROM dept
  WHERE d_name = 'RESEARCH'
);

SELECT e_name, sal
FROM emp
WHERE sal > (
  SELECT sal
  FROM emp
  WHERE e_name = 'ADAMS'
)
  AND sal < (
    SELECT sal
    FROM emp
    WHERE e_name = 'KING'
  )
;

SELECT d.d_name
FROM emp e
  RIGHT JOIN dept d
    USING(dept_no)
GROUP BY d.d_name
HAVING COUNT(e.e_name) = 0;

SELECT d1.d_name
FROM dept d1
  RIGHT JOIN emp e1
    USING (dept_no)
GROUP BY d1.d_name
HAVING COUNT(e1.e_name) = (
  SELECT MAX (COUNT(e.e_name)) AS max_count
  FROM emp e
    RIGHT JOIN dept d
      USING (dept_no)
  GROUP BY d.d_name
);

SELECT d1.d_name
FROM dept d1
  RIGHT JOIN emp e1
    USING (dept_no)
GROUP BY d1.d_name
HAVING SUM(e1.sal) = (
  SELECT MAX(SUM(e.sal)) AS max_count
  FROM emp e
    RIGHT JOIN dept d
      USING (dept_no)
  GROUP BY d.d_name
);

-- End of Lab 1
SELECT MAX(SUM(e.sal)) AS max_count
  FROM emp e
    RIGHT JOIN dept d
      USING (dept_no)
  GROUP BY d.d_name;

SELECT sal
  FROM emp
  WHERE e_name = 'ADAM';

SELECT *
FROM dual;

TRUNCATE TABLE emp;

SELECT *
FROM emp
FETCH FIRST 5 ROWS ONLY;

SELECT *
FROM dept;