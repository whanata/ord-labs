CREATE TABLE emp (
  emp_no NUMBER(4),
  e_name VARCHAR2(10),
  job VARCHAR2(9),
  mgr NUMBER(4),
  hire_date DATE,
  sal NUMBER(7, 2),
  comm NUMBER(7, 2),
  dept_no NUMBER(2)
);

CREATE TABLE dept (
  dept_no NUMBER(2),
  d_name VARCHAR2(14),
  loc VARCHAR2(14)
);

INSERT ALL
  INTO emp VALUES (7369, 'SMITH', 'CLERK', 7902, '17-DEC-1980', 800, NULL, 20)
  INTO emp VALUES (7499, 'ALLEN', 'SALESMAN', 7698, '20-FEB-1981', 1600, 300, 30)
  INTO emp VALUES (7521, 'WARD', 'SALESMAN', 7698, '22-FEB-1981', 1250, 500, 30)
  INTO emp VALUES (7566, 'JONES', 'MANAGER', 7839, '2-APR-1981', 2975, NULL, 20)
  INTO emp VALUES (7654, 'MARTIN', 'SALESMAN', 7698, '28-SEP-1981', 1250, 1400, 30)
  INTO emp VALUES (7698, 'BLAKE', 'MANAGER', 7839, '1-MAY-1981', 2850, NULL, 30)
  INTO emp VALUES (7782, 'CLARK', 'MANAGER', 7839, '9-JUN-1981', 2450, NULL, 10)
  INTO emp VALUES (7788, 'SCOTT', 'ANALYST', 7566, '09-DEC-1982', 3000, NULL, 20)
  INTO emp VALUES (7839, 'KING', 'PRESIDENT', NULL, '17-NOV-1981', 5000, NULL, 10)
  INTO emp VALUES (7844, 'TURNER', 'SALESMAN', 7698, '8-SEP-1981', 1500, 0, 30)
  INTO emp VALUES (7876, 'ADAMS', 'CLERK', 7788, '12-JAN-1983', 1100, NULL, 20)
  INTO emp VALUES (7900, 'JAMES', 'CLERK', 7698, '3-DEC-1981', 950, NULL, 30)
  INTO emp VALUES (7902, 'FORD', 'ANALYST', 7566, '3-DEC-1981', 3000, NULL, 20)
  INTO emp VALUES (7934, 'MILLER', 'CLERK', 7782, '23-JAN-1982', 1300, NULL, 10)
SELECT * FROM dual;

INSERT ALL
  INTO dept VALUES (10, 'ACCOUNTING', 'NEW YORK')
  INTO dept VALUES (20, 'RESEARCH', 'DALLAS')
  INTO dept VALUES (30, 'SALES', 'CHICAGO')
  INTO dept VALUES (40, 'OPERATIONS', 'BOSTON')
SELECT * FROM dual;

CREATE OR REPLACE TYPE department_type AS OBJECT (
  dept_no NUMBER(2),
  d_name VARCHAR2(14),
  loc VARCHAR2(14)
);

CREATE OR REPLACE TYPE employee_type AS OBJECT (
  emp_no NUMBER(4),
  e_name VARCHAR2(10),
  job VARCHAR2(9),
  mgr_ref REF EMPLOYEE_TYPE,
  hire_date DATE,
  sal NUMBER(7, 2),
  monthly_commission NUMBER(7, 2),
  dept_ref REF DEPARTMENT_TYPE
);

CREATE TABLE employee OF employee_type;
CREATE TABLE department OF department_type;

INSERT INTO department (dept_no, d_name, loc)
SELECT dept_no, d_name, loc
  FROM dept;

INSERT INTO employee (emp_no, e_name, job, hire_date, sal, monthly_commission, dept_ref)
SELECT e.emp_no, e.e_name, e.job,
  e.hire_date, e.sal, e.comm,
  (SELECT REF(d_ref) FROM department d_ref WHERE d_ref.dept_no = e.dept_no)
FROM emp e;

UPDATE employee e
SET e.mgr_ref = (
  SELECT REF(e_ref)
  FROM employee e_ref
    JOIN emp
      ON emp.mgr = e_ref.emp_no
  WHERE e.emp_no = emp.emp_no
);

SELECT e.emp_no, e.e_name, DEREF(e.dept_ref).d_name d_name
FROM employee e
WHERE DEREF(e.dept_ref).dept_no = 10;

SELECT e.emp_no, e.e_name, e.sal
FROM employee e
WHERE e.sal > DEREF(e.mgr_ref).sal;

CREATE OR REPLACE TYPE employee_type2 AS OBJECT (
  emp_no NUMBER(4),
  e_name VARCHAR2(10),
  job VARCHAR2(9),
  mgr_ref REF EMPLOYEE_TYPE,
  hire_date DATE,
  sal NUMBER(7, 2),
  monthly_commission NUMBER(7, 2),
  CONSTRUCTOR FUNCTION employee_type2 (
    emp_no NUMBER,
    e_name VARCHAR2,
    job VARCHAR2,
    hire_date DATE,
    sal NUMBER,
    monthly_commission NUMBER
  )
    RETURN SELF AS RESULT
);

CREATE OR REPLACE TYPE employee_table
AS TABLE OF employee_type2;

CREATE OR REPLACE TYPE department_type2 AS OBJECT (
  dept_no NUMBER(2),
  d_name VARCHAR2(14),
  loc VARCHAR2(14),
  employees employee_table
);

DROP TABLE department;

CREATE TABLE department OF department_type2
  NESTED TABLE employees STORE AS employee_data;

INSERT INTO department
SELECT dept_no,
  d_name,
  loc,
  CAST (
    MULTISET(
      SELECT emp_no,
        e_name,
        job,
        null,
        hire_date,
        sal,
        comm
      FROM emp
      WHERE emp.dept_no = dept.dept_no
    ) AS employee_table
  )
FROM dept;

SELECT e.emp_no
FROM TABLE (
  SELECT d.employees
  FROM department d
) e;

UPDATE TABLE (
  SELECT d.employees
  FROM department d
) e
SET mgr_ref = (
  SELECT REF(e_ref)
  FROM TABLE (
    SELECT d.employees
    FROM department d
  ) e_ref
    JOIN emp
      ON emp.mgr = e_ref.emp_no
  WHERE e.emp_no = emp.emp_no
);

SELECT REF(e)
FROM department d, TABLE(d.employees) e;

SELECT emp.emp_no, REF(e_ref)
  FROM employee e_ref
    JOIN emp
      ON emp.mgr = e_ref.emp_no;
-- End of Lab 2

SELECT e.emp_no
FROM TABLE (
  SELECT e_d.emp_no
  FROM department d, TABLE(d.employees) e_d
) e;

DROP TYPE employee_type;
DROP TYPE employee_table;
DROP TABLE EMPLOYEE_FILL;
TRUNCATE TABLE department;
DROP TABLE department;
DROP TABLE employee_type2;
DROP TYPE DEPARTMENT_TYPE2;

SELECT dept_no, d_name, loc, e.*
FROM department d, TABLE(d.employees) e;

SELECT REF(e_ref) FROM employee e_ref;

SELECT *
FROM employee;

SELECT *
FROM employee_fill;

SELECT *
FROM department;

TRUNCATE TABLE department;
TRUNCATE TABLE employee;

SELECT REF(e) FROM department e;

SELECT DEREF(dept_ref).d_name
FROM employee_fill;

SELECT *
FROM employee_fill
WHERE emp_no = 7369;

SELECT *
FROM emp;

select e.e_name, DEREF(dept_ref).d_name, DEREF(mgr_ref).e_name
from employee e;